// 1
let person = {
    name: "John",
    age: 30,
    gender: "male"
};

let newPerson = Object.assign({}, person);
newPerson.age = 35;

console.log(person);
console.log(newPerson);


// 2
let car = {
    make: 'Toyota',
    model: 'Camry',
    year: 2015
};

function printCarInfo(car) {
    console.log(`Make: ${car.make}, Model: ${car.model}, Year: ${car.year}`);

    if (car.year < 2001) {
        console.log('Машина занадто стара.');
    }
}
printCarInfo(car);

// 3
let str = 'JavaScript is a high-level programming language';

function countWords(str) {
    let words = str.split(' ');
    return words.length;
}

console.log(countWords(str));

// 4
let string = 'JavaScript is awesome!';

function reverseString(string) {
    let chars = string.split('');
    let reversedChars = chars.reverse();
    let reversedStr = reversedChars.join('');
    return reversedStr;
}

console.log(reverseString(string));

// 5
let str_2 = 'JavaScript is awesome!'

function reverseWordsInString(str_2) {
    let words = str_2.split(' ');
    let reversedWords = words.map(function (word) {
        return word.split('').reverse().join('');
    });
    let reversedStr = reversedWords.join(' ');
    return reversedStr;
}

console.log(reverseWordsInString(str_2));
